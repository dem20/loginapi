import './App.css';
import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';

function App() {
  const errRef = useRef();

  const [email, setEmail] = useState('');
  const [password, setPassWord] = useState('');
  const [errMsg, setErrMsg] = useState('');
  const [success, setSuccess] = useState(false);

  async function login(){
      try {
        let item={email,password};
        let result = await axios.post("https://api-nodejs-todolist.herokuapp.com/user/login",
        JSON.stringify(item),
        {
          headers:{
            "Content-Type" : "application/json"
        }
      });
      
      // console.log(JSON.stringify(result?.data));
      setSuccess(true);
      } catch (err) {
        if(!err?.result){
          setErrMsg('Login failed try again');
        }
        setEmail("");
        setPassWord("");
      }
  }

  return (
    <>
      {success ? (
        <div>
          <h1>Login success</h1>
        </div>
      ) : (
    <section>
      <h1>Login</h1>
      <p ref={errRef} className={errMsg ? "errMsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
      <input 
        type='text' 
        placeholder='Email' 
        onChange={(e)=>setEmail(e.target.value)} 
        value={email}
        className='form-control'
      />
      <br/>
      <input 
        type='password' 
        placeholder='Password' 
        onChange={(e)=>setPassWord(e.target.value)} 
        value = {password}
        className='form-control'
      />
      <br/>
      <button onClick={login}>Login</button>
     </section>
     )}
     </>
  );
}

export default App;
